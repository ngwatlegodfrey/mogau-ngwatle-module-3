import 'package:flutter/material.dart';
import 'package:mogau_ngwatle_module_3/componets/button.dart';
import 'package:mogau_ngwatle_module_3/componets/featureBtn.dart';
import 'package:mogau_ngwatle_module_3/screens/featureScreen1.dart';
import 'package:mogau_ngwatle_module_3/screens/featureScreen2.dart';
import 'package:mogau_ngwatle_module_3/screens/user_profile_edit.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DASHBOARD"),
        centerTitle: true,
        backgroundColor: Color(0xffFFCB00),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FeatureBtn(
              text: "SKILLS",
              color: const Color(0xffFFCB00),
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return const FeatureScreen1();
                  }),
                );
              },
            ),
            const SizedBox(height: 20),
            FeatureBtn(
              text: "SUBJECTS",
              color: const Color(0xffFFCB00),
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return const FeatureScreen2();
                  }),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return UserProfileEdit();
              },
            ),
          );
        },
        backgroundColor: Color(0xffFFCB00),
        child: const Icon(Icons.edit),
      ),
    );
  }
}
