import 'package:flutter/material.dart';
import 'package:mogau_ngwatle_module_3/componets/cards.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SKILLS"),
        centerTitle: true,
        backgroundColor: const Color(0xffFFCB00),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const Text(
              "MY SKILLS",
              style: TextStyle(
                color: Color(0xff000001),
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            Cards(
              text: "Angular",
              color: Colors.red,
            ),
            Cards(
              text: "IONIC",
              color: Colors.lightBlue,
            ),
            Cards(
              text: "FIREBASE",
              color: Colors.orange,
            ),
            Cards(
              text: "FLUTTER",
              color: Colors.blue,
            ),
            Cards(
              text: "C#",
              color: Colors.green,
            ),
            Cards(
              text: "Nodejs",
              color: Colors.black,
            ),
            Cards(
              text: "BOOTSTRAP",
              color: Colors.purple,
            ),
          ],
        ),
      ),
    );
  }
}
